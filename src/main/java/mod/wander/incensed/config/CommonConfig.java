package mod.wander.incensed.config;

import mod.wander.incensed.Incensed;
import mod.wander.incensed.util.collection.Bag;
import mod.wander.incensed.util.collection.HashBag;
import mod.wander.incensed.util.collection.MutableBag;
import net.minecraft.entity.EntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistry;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public final class CommonConfig {

    private static final HashBag<ResourceLocation> ENTITY_BLACKLIST = new HashBag<>();
    private static final HashBag<ResourceLocation> DIMENSION_WHITELIST = new HashBag<>();

    private static final List<ResourceLocation> VANILLA_DIMENSIONS = Arrays.asList(
            new ResourceLocation("minecraft:overworld"),
            new ResourceLocation("minecraft:the_nether"),
            new ResourceLocation("minecraft:the_end")
    );

    private static final List<ResourceLocation> DEFAULT_DIMENSION_WHITELIST = Arrays.asList(
            new ResourceLocation("minecraft:overworld")
    );


    public final ForgeConfigSpec.ConfigValue<Integer> effectRadius;
    public final ForgeConfigSpec.ConfigValue<Boolean> allowSpawners;
    private final ForgeConfigSpec.ConfigValue<List<? extends String>> entityBlacklistOverrides;
    private final ForgeConfigSpec.ConfigValue<List<? extends String>> dimensionWhitelistOverrides;
    public final ForgeConfigSpec.ConfigValue<Boolean> placeBlockOnDeny;

    public final Bag<ResourceLocation> entityBlacklist = ENTITY_BLACKLIST;
    public final Bag<ResourceLocation> dimensionWhitelist = DIMENSION_WHITELIST;

    CommonConfig(ForgeConfigSpec.Builder builder) {
        builder.push("General");

        effectRadius = builder
                .comment(" The radius of the spherical area of effect")
                //.translation("")
                .defineInRange("effectRadius", 48, 0, Integer.MAX_VALUE);

        allowSpawners = builder
                .comment(" Setting this to false will also block spawning from mob spawners")
                .define("allowSpawners", true);

        entityBlacklistOverrides = builder
                .comment(
                        " Use this to modify the default blacklist for entity spawning",
                        " The + prefix will add the entity to the blacklist",
                        " The - prefix will remove the entity from the blacklist",
                        " Note: Each entry needs to be put in quotes! Multiple Entries should be separated by comma.",
                        " Deny cows: \"+minecraft:cow\"",
                        " Allow creepers: \"-minecraft:creeper\""
                )
                .defineList("entityBlacklistOverrides", new ArrayList<>(), e -> e instanceof String);

        dimensionWhitelistOverrides = builder
                .comment(
                        " Use this to modify the default whitelist for dimensions that",
                        " allow the placement of incense burners",
                        " The + prefix will add the dimension to the whitelist",
                        " The - prefix will remove the dimension from the whitelist",
                        " Note: Each entry needs to be put in quotes! Multiple Entries should be separated by comma.",
                        " Allow the nether: \"+minecraft:the_nether\"",
                        " Deny overworld: \"-minecraft:overworld\""

                )
                .defineList("dimensionWhitelistOverrides", new ArrayList<>(), e -> e instanceof String);


        builder.pop();

        builder.push("Debug");

        placeBlockOnDeny = builder
                .comment(" Place a red wool block wherever an entity spawn is denied.")
                .define("placeBlockOnDeny", false);

        builder.pop();
    }

    void onFileChange(final ModConfig.Reloading configEvent) {
        Incensed.LOG.debug("updating common config");

        ENTITY_BLACKLIST.clear();
        addHostileEntities(ENTITY_BLACKLIST);
        applyOverrides(ENTITY_BLACKLIST,
                entityBlacklistOverrides.get(),
                ForgeRegistries.ENTITIES::containsKey);

        DIMENSION_WHITELIST.clear();
        DIMENSION_WHITELIST.addAll(DEFAULT_DIMENSION_WHITELIST);
        applyOverrides(DIMENSION_WHITELIST,
                dimensionWhitelistOverrides.get(),
                rl -> VANILLA_DIMENSIONS.contains(rl) ||
                        ForgeRegistries.MOD_DIMENSIONS.containsKey((rl))
        );
    }

    private static void addHostileEntities(MutableBag<ResourceLocation> bag) {
        IForgeRegistry<EntityType<?>> reg = ForgeRegistries.ENTITIES;
        reg.getKeys().stream()
                .map(rl -> Pair.of(rl, reg.getValue(rl)))
                .filter(e -> e.getValue() != null) // some resource locations point to null types?
                .filter(e -> !e.getValue().getClassification().getPeacefulCreature())
                .forEach(e -> bag.add(e.getKey()));
    }

    private static void applyOverrides(
            MutableBag<ResourceLocation> bag,
            List<? extends String> overrides,
            Function<ResourceLocation, Boolean> validator) {

        for (String override : overrides) {
            // minimum len is prefix + valid resource location, i.e. +a:b
            if (override.length() < 4) continue;

            char prefix = override.charAt(0);
            ResourceLocation rl = new ResourceLocation(override.substring(1));

            switch (prefix) {
                case '+':
                    if (!bag.contains(rl)) {
                        if (!validator.apply(rl)) {
                            Incensed.LOG.warn(" Override list contains unregistered resource '{}', skipping", rl);
                            continue;
                        }
                        bag.add(rl);
                        Incensed.LOG.info("  Added '{}' to override list", rl);
                    }
                    break;
                case '-':
                    if (bag.removeIf(rrl -> rrl.equals(rl))) {
                        Incensed.LOG.info("  Removed '{}' from override list", rl);
                    }
                    break;
                default:
                    Incensed.LOG.warn("  Invalid override list prefix: '{}', only + and - are valid prefixes", rl);
                    break;
            }
        }
    }
}
