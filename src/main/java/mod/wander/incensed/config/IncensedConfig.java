package mod.wander.incensed.config;

import mod.wander.incensed.Incensed;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import org.apache.commons.lang3.tuple.Pair;

@Mod.EventBusSubscriber(
        modid = Incensed.MOD_ID,
        bus = Mod.EventBusSubscriber.Bus.MOD)
public final class IncensedConfig {

    private static final ForgeConfigSpec COMMON_SPEC;
    public static final CommonConfig COMMON;

    static {
        final Pair<CommonConfig, ForgeConfigSpec> pair = new ForgeConfigSpec.Builder()
                .configure(CommonConfig::new);

        COMMON_SPEC = pair.getValue();
        COMMON = pair.getKey();
    }

    public static void init() {
        ModLoadingContext.get().registerConfig(
                ModConfig.Type.COMMON,
                IncensedConfig.COMMON_SPEC,
                "incensed.toml");
    }

    @SubscribeEvent
    public static void onFileChange(final ModConfig.Reloading configEvent) {
        ModConfig.Type type = configEvent.getConfig().getType();

        if (type == ModConfig.Type.COMMON) {
            COMMON.onFileChange(configEvent);
        }
    }
}
