package mod.wander.incensed.interceptor;

import mod.wander.incensed.config.IncensedConfig;
import mod.wander.incensed.util.IRangeStrategy;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.util.math.BlockPos;

public class EventInterceptor {

    private static final int NBT_VERSION = 1;
    private static final String VERSION_KEY = "1";
    private static final String POS_KEY = "pos";

    public static CompoundNBT serialize(EventInterceptor interceptor) {
        if (interceptor == null) throw new IllegalArgumentException();

        CompoundNBT nbt = new CompoundNBT();
        nbt.putInt(VERSION_KEY, NBT_VERSION);
        nbt.put(POS_KEY, NBTUtil.writeBlockPos(interceptor.pos));

        return nbt;
    }

    public static EventInterceptor deserialize(CompoundNBT nbt) {
        if (nbt == null) throw new IllegalArgumentException();
        int version = nbt.getInt(VERSION_KEY);
        BlockPos pos = NBTUtil.readBlockPos(nbt.getCompound(POS_KEY));
        return new EventInterceptor(pos);
    }

    private final BlockPos pos;

    public EventInterceptor(BlockPos pos) {
        this.pos = new BlockPos(pos);
    }

    public boolean shouldDenySpawn(Entity entity) {
        return shouldDenySpawnByPos(entity) &&
                shouldDenySpawnByType(entity);
    }

    public BlockPos getPos() {
        return pos;
    }

    public CompoundNBT serialize() {
        return serialize(this);
    }

    private boolean shouldDenySpawnByPos(Entity entity) {
        return (IRangeStrategy.Spherical.isPositionInRange(
                entity.lastTickPosX,
                entity.lastTickPosY,
                entity.lastTickPosZ,
                pos,
                IncensedConfig.COMMON.effectRadius.get()
        ));
    }

    private boolean shouldDenySpawnByType(Entity entity) {
        return IncensedConfig.COMMON.entityBlacklist.contains(entity.getType().getRegistryName());
    }

    @Override
    public String toString() {
        return "EventInterceptor{" +
                "pos=" + pos +
                '}';
    }
}
