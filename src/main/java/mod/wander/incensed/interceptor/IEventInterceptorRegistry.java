package mod.wander.incensed.interceptor;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.World;
import net.minecraftforge.common.util.INBTSerializable;

public interface IEventInterceptorRegistry extends INBTSerializable<CompoundNBT> {

    boolean shouldDenySpawn(Entity entity);

    void registerInterceptor(String key, EventInterceptor effect);

    void unregisterInterceptor(String key);

    EventInterceptor getInterceptor(String key);

    void onGlobalTick(World world);

    IEventInterceptorRegistry VOID = new IEventInterceptorRegistry() {

        @Override
        public CompoundNBT serializeNBT() {
            return new CompoundNBT();
        }

        @Override
        public void deserializeNBT(CompoundNBT nbt) {
        }

        @Override
        public boolean shouldDenySpawn(Entity entity) {
            return false;
        }

        @Override
        public void registerInterceptor(String key, EventInterceptor effect) {
        }

        @Override
        public void unregisterInterceptor(String key) {
        }

        @Override
        public EventInterceptor getInterceptor(String key) {
            return null;
        }

        @Override
        public void onGlobalTick(World world) {
        }
    };
}
