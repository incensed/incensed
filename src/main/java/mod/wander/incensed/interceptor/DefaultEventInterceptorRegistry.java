package mod.wander.incensed.interceptor;

import mod.wander.incensed.Incensed;
import mod.wander.incensed.init.ModCaps;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;

class DefaultEventInterceptorRegistry implements IEventInterceptorRegistry {

    private final HashMap<String, EventInterceptor> interceptors = new HashMap<>();
    private String[] index = new String[0];

    DefaultEventInterceptorRegistry() {
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT nbt = new CompoundNBT();
        //noinspection ForLoopReplaceableByForEach
        for (int i = 0; i < index.length; i++) {
            String key = index[i];
            EventInterceptor value = interceptors.get(key);
            nbt.put(key, value.serialize());
        }
        Incensed.LOG.debug("Saved {} interceptors", interceptors.size());
        return nbt;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        interceptors.clear();
        for (String key : nbt.keySet()) {
            CompoundNBT entry = nbt.getCompound(key);
            EventInterceptor value = EventInterceptor.deserialize(entry);
            Incensed.LOG.debug("Restored {}", value);
            interceptors.put(key, value);
        }
        indexMap();
        Incensed.LOG.debug("Restored {} interceptors", interceptors.size());
    }

    @Override
    public boolean shouldDenySpawn(Entity entity) {
        //noinspection ForLoopReplaceableByForEach
        for (int i = 0; i < index.length; i++) {
            String key = index[i];
            EventInterceptor value = interceptors.get(key);
            if (value.shouldDenySpawn(entity)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void registerInterceptor(String key, EventInterceptor interceptor) {
        interceptors.put(key, interceptor);
        indexMap();
        Incensed.LOG.debug("Registered {}: {} ({} total)", key, interceptor, interceptors.size());
    }

    @Override
    public void unregisterInterceptor(String key) {
        interceptors.remove(key);
        indexMap();
        Incensed.LOG.debug("Unregistered {} ({} total)", key, interceptors.size());
    }

    private void indexMap() {
        index = interceptors.keySet().toArray(new String[0]);
    }

    @Override
    public EventInterceptor getInterceptor(String key) {
        return interceptors.get(key);
    }

    @Override
    public void onGlobalTick(World world) {

    }

    public static class Provider implements ICapabilityProvider, ICapabilitySerializable<CompoundNBT> {
        private final IEventInterceptorRegistry instance = new DefaultEventInterceptorRegistry();

        @SuppressWarnings("rawtypes")
        private final LazyOptional holder = LazyOptional.of(() -> instance);

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            if (cap == ModCaps.EVENT_INTERCEPTOR_REGISTRY) {
                //noinspection unchecked
                return holder;
            }
            return LazyOptional.empty();
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap) {
            return getCapability(cap, null);
        }

        @Override
        public CompoundNBT serializeNBT() {
            return instance.serializeNBT();
        }

        @Override
        public void deserializeNBT(CompoundNBT nbt) {
            instance.deserializeNBT(nbt);
        }
    }
}
