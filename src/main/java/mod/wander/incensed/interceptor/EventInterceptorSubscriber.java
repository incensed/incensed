package mod.wander.incensed.interceptor;

import mod.wander.incensed.Incensed;
import mod.wander.incensed.config.IncensedConfig;
import mod.wander.incensed.init.ModCaps;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = Incensed.MOD_ID)
public final class EventInterceptorSubscriber {

    private static final String CHECK_SPAWN_EVENT_PROFILE = "incensed:on_check_spawn";

    private EventInterceptorSubscriber() {
        throw new UnsupportedOperationException();
    }

    private static int spawnDeniedCount = 0;
    private static int statLoggingTickCount = 0;
    private static final int STAT_LOGGING_INTERVAL = 1024;

    @SubscribeEvent
    public static void onWorldAttachCapabilityEvent(AttachCapabilitiesEvent<World> event) {
        if (event.getObject().isRemote) return;
        Incensed.LOG.debug("onWorldAttachCapabilityEvent: " + event.getObject().getDimension().getType().getRegistryName());
        event.addCapability(new ResourceLocation(Incensed.MOD_ID, "registry"),
                new DefaultEventInterceptorRegistry.Provider());
    }

    @SubscribeEvent
    public static void onCheckSpawn(LivingSpawnEvent.CheckSpawn event) throws InterruptedException {
        if (event.getResult() == Event.Result.ALLOW) return;
        if (IncensedConfig.COMMON.allowSpawners.get() && event.isSpawner()) return;
        Entity entity = event.getEntity();
        World world = entity.getEntityWorld();

        world.getProfiler().startSection(CHECK_SPAWN_EVENT_PROFILE);
        world.getCapability(ModCaps.EVENT_INTERCEPTOR_REGISTRY)
                .ifPresent(reg -> {
                    if (reg.shouldDenySpawn(entity)) {
                        spawnDeniedCount++;
                        event.setResult(Event.Result.DENY);

                        if (IncensedConfig.COMMON.placeBlockOnDeny.get()) {
                            BlockPos entityPos = new BlockPos(
                                    entity.lastTickPosX,
                                    entity.lastTickPosY,
                                    entity.lastTickPosZ
                            );
                            world.setBlockState(entityPos, Blocks.RED_WOOL.getDefaultState());
                        }
                    }
                });
        world.getProfiler().endSection();
    }

    @SubscribeEvent
    public static void onGlobalTick(TickEvent.ServerTickEvent event) {
        if (event.phase == TickEvent.Phase.END && Incensed.isServerStarted()) {

            if (++statLoggingTickCount >= STAT_LOGGING_INTERVAL) {
                Incensed.LOG.debug("Denied {} spawns in {} ticks", spawnDeniedCount, statLoggingTickCount);
                statLoggingTickCount = 0;
                spawnDeniedCount = 0;
            }

            for (ServerWorld world : Incensed.getServer().getWorlds()) {
                world.getCapability(ModCaps.EVENT_INTERCEPTOR_REGISTRY)
                        .ifPresent(reg -> {
                            reg.onGlobalTick(world);
                        });
            }
        }
    }
}

