package mod.wander.incensed.tileentity;

import mod.wander.incensed.block.IncenseBurnerBlock;
import mod.wander.incensed.init.ModTileEntityTypes;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Random;


public class IncenseBurnerTileEntity extends TileEntity implements ITickableTileEntity {

    public IncenseBurnerTileEntity(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
    }

    public IncenseBurnerTileEntity() {
        this(ModTileEntityTypes.INCENSE_BURNER.get());
    }

    @Override
    public void tick() {
        World world = getWorld();
        if (world != null && world.isRemote) {
            addParticles();
        }
    }

    private void addParticles() {
        World world = getWorld();
        if (world != null) {
            BlockPos blockpos = getPos();
            Random random = world.rand;
            if (random.nextFloat() < 0.11F) {
                for (int i = 0; i < random.nextInt(2) + 2; ++i) {
                    IncenseBurnerBlock.spawnSmokeParticles(world, blockpos);
                }
            }
        }
    }
}
