package mod.wander.incensed.util;

import net.minecraft.util.math.BlockPos;

public interface IRangeStrategy {
    boolean isPositionInRange(double posX, double posY, double posZ, BlockPos blockPos, int range);

    IRangeStrategy Cubic = (posX, posY, posZ, blockPos, range) -> {
        // a range of 0 is effectively a 1x1x1 block volume, the blockPos itself.
        double minX = blockPos.getX() - range;
        double minY = blockPos.getY() - range;
        double minZ = blockPos.getZ() - range;
        double maxX = blockPos.getX() + range + 1;
        double maxY = blockPos.getY() + range + 1;
        double maxZ = blockPos.getZ() + range + 1;
        return minX <= posX && maxX >= posX &&
                minY <= posY && maxY >= posY &&
                minZ <= posZ && maxZ >= posZ;
    };

    IRangeStrategy Cylinder = (posX, posY, posZ, blockPos, range) -> {
        double dx = blockPos.getX() + 0.5 - posX;
        double dy = Math.abs(blockPos.getY() + 0.5 - posY);
        double dz = blockPos.getZ() + 0.5 - posZ;
        return (dx * dx + dz * dz) <= range && dy <= range;
    };

    IRangeStrategy Spherical = (posX, posY, posZ, blockPos, range) -> {
        double dx = blockPos.getX() + 0.5 - posX;
        double dy = Math.abs(blockPos.getY() + 0.5 - posY);
        double dz = blockPos.getZ() + 0.5 - posZ;
        return (dx * dx) + (dy * dy) + (dz * dz) < (range * range);
    };
}