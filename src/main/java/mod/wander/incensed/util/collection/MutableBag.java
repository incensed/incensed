package mod.wander.incensed.util.collection;

import java.util.Collection;
import java.util.function.Predicate;

public interface MutableBag<T> extends Bag<T> {
    boolean add(T element);
    boolean addAll(Collection<T> elements);
    boolean remove(T element);

    boolean removeIf(Predicate<? super T> predicate);

    void clear();
}
