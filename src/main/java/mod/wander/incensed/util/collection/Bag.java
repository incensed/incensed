package mod.wander.incensed.util.collection;

public interface Bag<T> {
    int size();

    boolean contains(T element);

    T[] toArray();
}
