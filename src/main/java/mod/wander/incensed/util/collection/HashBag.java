package mod.wander.incensed.util.collection;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.function.Predicate;

public class HashBag<T> implements MutableBag<T> {

    protected final HashSet<T> elements = new HashSet<>();

    public HashBag() {
    }

    public HashBag(T[] elements) {
        this.elements.addAll(Arrays.asList(elements));
    }

    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public boolean contains(T element) {
        return elements.contains(element);
    }

    @Override
    public boolean add(T element) {
        return elements.add(element);
    }

    @Override
    public boolean addAll(Collection<T> elements) {
        return this.elements.addAll(elements);
    }

    @Override
    public boolean remove(T element) {
        return elements.remove(element);
    }

    @Override
    public boolean removeIf(Predicate<? super T> predicate) {
        return elements.removeIf(predicate);
    }

    @Override
    public void clear() {
        elements.clear();
    }

    @SuppressWarnings("unchecked")
    @Override
    public T[] toArray() {
        return (T[]) elements.toArray();
    }
}
