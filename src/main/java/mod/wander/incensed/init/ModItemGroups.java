package mod.wander.incensed.init;

import mod.wander.incensed.Incensed;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

import javax.annotation.Nonnull;
import java.util.function.Supplier;

public class ModItemGroups {

    public static final ItemGroup DEFAULT_GROUP = new ModItemGroup(Incensed.MOD_ID,
            () -> new ItemStack(ModBlocks.INCENSE_BURNER.get()));

    public static class ModItemGroup extends ItemGroup {

        private final Supplier<ItemStack> iconSupplier;

        public ModItemGroup(final String name, final Supplier<ItemStack> iconSupplier) {
            super(name);
            this.iconSupplier = iconSupplier;
        }

        @Override
        @Nonnull
        public ItemStack createIcon() {
            return iconSupplier.get();
        }
    }
}
