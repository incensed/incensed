package mod.wander.incensed.init;

import mod.wander.incensed.Incensed;
import mod.wander.incensed.tileentity.IncenseBurnerTileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public final class ModTileEntityTypes {

    public static final DeferredRegister<TileEntityType<?>> TILE_ENTITY_TYPES = new DeferredRegister(ForgeRegistries.TILE_ENTITIES, Incensed.MOD_ID);

    @SuppressWarnings("ConstantConditions")
    public static final RegistryObject<TileEntityType<IncenseBurnerTileEntity>> INCENSE_BURNER = TILE_ENTITY_TYPES.register(
            "incense_burner",
            () -> TileEntityType.Builder.create(
                    IncenseBurnerTileEntity::new,
                    ModBlocks.INCENSE_BURNER.get())
                    .build(null)
    );
}
