package mod.wander.incensed.init;

import mod.wander.incensed.interceptor.IEventInterceptorRegistry;
import mod.wander.incensed.util.GenericNBTStorage;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;

public class ModCaps {

    @CapabilityInject(IEventInterceptorRegistry.class)
    public static Capability<IEventInterceptorRegistry> EVENT_INTERCEPTOR_REGISTRY = null;

    public static void register() {
        CapabilityManager.INSTANCE.register(
                IEventInterceptorRegistry.class,
                new GenericNBTStorage<>(),
                () -> IEventInterceptorRegistry.VOID);
    }
}
