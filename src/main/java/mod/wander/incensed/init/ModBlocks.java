package mod.wander.incensed.init;

import mod.wander.incensed.Incensed;
import mod.wander.incensed.block.IncenseBurnerBlock;
import mod.wander.incensed.interceptor.EventInterceptor;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public final class ModBlocks {

    public static final DeferredRegister<Block> BLOCKS = new DeferredRegister(ForgeRegistries.BLOCKS, Incensed.MOD_ID);

    public static final RegistryObject<Block> INCENSE_BURNER = BLOCKS.register(
            "incense_burner",
            () -> new IncenseBurnerBlock(Block.Properties.create(Material.IRON)
                    .hardnessAndResistance(3.5F)
                    .sound(SoundType.LANTERN)
                    .lightValue(10)
                    .notSolid(),
                    pos -> pos.getX() + "_" + pos.getY() + "_" + pos.getZ(),
                    EventInterceptor::new)
    );
}
