package mod.wander.incensed;

import mod.wander.incensed.config.IncensedConfig;
import mod.wander.incensed.init.ModBlocks;
import mod.wander.incensed.init.ModCaps;
import mod.wander.incensed.init.ModItems;
import mod.wander.incensed.init.ModTileEntityTypes;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.server.FMLServerStartedEvent;
import net.minecraftforge.fml.event.server.FMLServerStoppingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nonnull;

@Mod(Incensed.MOD_ID)
public class Incensed {
    public static final String MOD_ID = "incensed";
    public static final Logger LOG = LogManager.getLogger();

    private static MinecraftServer SERVER = null;

    public Incensed() {
        MinecraftForge.EVENT_BUS.register(this);
        final IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
        bus.addListener(this::setup);
        bus.register(this);

        ModBlocks.BLOCKS.register(bus);
        ModItems.ITEMS.register(bus);
        ModTileEntityTypes.TILE_ENTITY_TYPES.register(bus);

        IncensedConfig.init();
    }


    private void setup(final FMLCommonSetupEvent event) {
        ModCaps.register();
    }

    @SubscribeEvent
    public void onServerStarting(FMLServerStartedEvent event) {
        LOG.debug("onServerStarting");
        SERVER = event.getServer();
    }

    @SubscribeEvent
    public void onServerStopping(FMLServerStoppingEvent event) {
        LOG.debug("onServerStopping");
        SERVER = null;
    }

    public static boolean isServerStarted() {
        return SERVER != null;
        // register command managers
    }

    @Nonnull
    public static MinecraftServer getServer() {
        if (!isServerStarted()) throw new IllegalStateException("Server not started!");
        return SERVER;
    }
}
