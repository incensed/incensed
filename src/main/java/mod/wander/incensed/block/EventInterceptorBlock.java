package mod.wander.incensed.block;

import mod.wander.incensed.init.ModCaps;
import mod.wander.incensed.interceptor.EventInterceptor;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import java.util.function.Function;

public class EventInterceptorBlock extends Block {

    private Function<BlockPos, String> keyFactory;
    private Function<BlockPos, EventInterceptor> interceptorFactory;

    public EventInterceptorBlock(Properties properties,
                                 Function<BlockPos, String> keyFactory,
                                 Function<BlockPos, EventInterceptor> interceptorFactory) {
        super(properties);
        this.keyFactory = keyFactory;
        this.interceptorFactory = interceptorFactory;
    }

    @Override
    public void onBlockAdded(@Nonnull BlockState state, @Nonnull World worldIn, @Nonnull BlockPos pos, @Nonnull BlockState oldState, boolean isMoving) {
        super.onBlockAdded(state, worldIn, pos, oldState, isMoving);
        worldIn.getCapability(ModCaps.EVENT_INTERCEPTOR_REGISTRY)
                .ifPresent(reg -> {
                    reg.registerInterceptor(
                            keyFactory.apply(pos),
                            interceptorFactory.apply(pos)
                    );
                });
    }

    @Override
    public void onReplaced(@Nonnull BlockState state, @Nonnull World worldIn, @Nonnull BlockPos pos, @Nonnull BlockState newState, boolean isMoving) {
        worldIn.getCapability(ModCaps.EVENT_INTERCEPTOR_REGISTRY)
                .ifPresent(reg -> {
                    reg.unregisterInterceptor(keyFactory.apply(pos));
                });

        super.onReplaced(state, worldIn, pos, newState, isMoving);
    }
}
